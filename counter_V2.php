<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 25.02.2020
 * Time: 18:25
 */

echo "Counter_v2 \n";

$endLimit = 150;
$sum_a_b = 0;
$sum_a_b_c = 0;
$sum_a_b_c_d = 0;
$array = array();
$i = 5;

for ($i = 1; $i <= $endLimit; $i++) {
    $array[] = pow($i, 5);
}

$start = time();

for ($a = 25; $a < $endLimit - 4; $a++) {
    echo 'a - ' . $a . PHP_EOL;
    for ($b = $a + 1; $b < $endLimit - 3; $b++) {
        $sum_a_b = $array[$a] + $array[$b];
        for ($c = $b + 1; $c < $endLimit - 2; $c++) {
            $sum_a_b_c = $sum_a_b + $array[$c];
            for ($d = $c + 1; $d < $endLimit - 1; $d++) {
                $sum_a_b_c_d = $sum_a_b_c + $array[$d];
                do {
                    if ($sum_a_b_c_d === $array[$i]) {
                        echo "1 = ", $a + 1, PHP_EOL, "2 = ", $b + 1, PHP_EOL, "3 = ", $c + 1, PHP_EOL, "4 = ", $d + 1, PHP_EOL, "5 = ", $i + 1, PHP_EOL;
                    }
                    $i++;
                } while (($sum_a_b_c_d >= $array[$i]) and ($i < $endLimit));
            }
        }
    }
}

$end = time();

echo "Time passed " . ($end - $start);